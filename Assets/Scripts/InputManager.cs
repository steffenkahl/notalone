﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace NotAlone
{
    public class InputManager : MonoBehaviour
    {
        private DialogueManager dialogueManager;

        private void Start()
        {
            dialogueManager = GameObject.FindGameObjectWithTag(GameTags.DIALOGUEMANAGER).GetComponent<DialogueManager>();
        }

        public void OnContinue(InputAction.CallbackContext ctx)
        {
            if (ctx.started)
            {
                if (dialogueManager.DialogueActive)
                {
                    dialogueManager.NextMessage();
                }
                else
                {
                    //Start Dialogue vie Input Module
                }
            }
        }
    }
}