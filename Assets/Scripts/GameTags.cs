﻿namespace NotAlone
{
    public static class GameTags
    {
        public static string CLICKABLEMANAGER = "ClickableManager";
        public static string DIALOGUEMANAGER = "DialogueManager";
        public static string INPUTMANAGER = "InputManager";
        public static string CLICKABLEPOINT = "ClickablePoint";
    }
}