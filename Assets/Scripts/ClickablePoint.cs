using System;
using System.Collections;
using System.Collections.Generic;
using NotAlone;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace NotAlone
{
    public class ClickablePoint : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Color defaultColor;
        [SerializeField] private Color onHoverColor;
        [SerializeField] private List<Dialogue> clickableDialogue;
        private SpriteRenderer renderer;
        private DialogueManager dialogueManager;

        private void Start()
        {
            dialogueManager = GameObject.FindGameObjectWithTag(GameTags.DIALOGUEMANAGER).GetComponent<DialogueManager>();
            renderer = GetComponent<SpriteRenderer>();
            renderer.color = defaultColor;
        }

        public List<Dialogue> GetDialogue()
        {
            return clickableDialogue;
        }
        
        public void OnPointerClick (PointerEventData eventData)
        {
            dialogueManager.StartDialogue(GetDialogue());
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            renderer.color = onHoverColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            renderer.color = defaultColor;
        }

        private void OnValidate()
        {
            renderer = GetComponent<SpriteRenderer>();
            renderer.color = defaultColor;
        }
    }
}