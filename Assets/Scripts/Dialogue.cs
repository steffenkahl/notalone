﻿using System;
using UnityEngine;

namespace NotAlone
{
    [Serializable]
    public class Dialogue
    {
        [SerializeField] private string headline;
        [SerializeField] private string message;
        [SerializeField] private DialogueAction actionAfterText;
        [SerializeField] private string sceneToLoadAfter;

        public string GetHeadline()
        {
            return headline;
        }

        public string GetMessage()
        {
            return message;
        }

        public DialogueAction GetAction()
        {
            return actionAfterText;
        }

        public string GetSceneToLoadAfter()
        {
            return sceneToLoadAfter;
        }
    }

    public enum DialogueAction
    {
        NONE,
        NEXTSCENE,
        NEXTDIALOGUE,
        SHOWCLICKABLES
    }
}