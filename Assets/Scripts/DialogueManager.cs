using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NotAlone
{
    public class DialogueManager : MonoBehaviour
    {
        [SerializeField] private List<Dialogue> dialogues;
        [SerializeField] private int currentDialogueStep;
        [SerializeField] private Dialogue currentDialogue;
        [SerializeField] private TMP_Text headline;
        [SerializeField] private TMP_Text message;
        [SerializeField] private GameObject dialogueBox;
        [SerializeField] private bool dialogueActive;

        private ClickableManager clickableManager;

        public bool DialogueActive
        {
            get => dialogueActive;
            set => dialogueActive = value;
        }

        private void Start()
        {
            clickableManager = GameObject.FindGameObjectWithTag(GameTags.CLICKABLEMANAGER).GetComponent<ClickableManager>();
        }

        public void StartDialogue(List<Dialogue> dialoguesIn)
        {
            dialogues = dialoguesIn;
            currentDialogueStep = 0;
            currentDialogue = dialogues[currentDialogueStep];
            headline.text = currentDialogue.GetHeadline();
            message.text = currentDialogue.GetMessage();
            dialogueBox.SetActive(true);
            dialogueActive = true;
            clickableManager.ShowClickables(false);
        }

        public void NextMessage()
        {
            DialogueAction action = currentDialogue.GetAction();
            switch (action)
            {
                default:
                    //Nothing
                case DialogueAction.NEXTSCENE:
                    SceneManager.LoadScene(currentDialogue.GetSceneToLoadAfter());
                    break;
                case DialogueAction.NEXTDIALOGUE:
                    currentDialogueStep += 1;
                    if (dialogues[currentDialogueStep] != null)
                    {
                        currentDialogue = dialogues[currentDialogueStep];
                        headline.text = currentDialogue.GetHeadline();
                        message.text = currentDialogue.GetMessage();
                    }
                    else
                    {
                        headline.text = "ERROR";
                        message.text = "Message Index out of bounds";
                    }
                    break;
                case DialogueAction.SHOWCLICKABLES:
                    clickableManager.ShowClickables();
                    headline.text = "";
                    message.text = "";
                    dialogueBox.SetActive(false);
                    dialogueActive = false;
                    break;
            }
        }
    }
}