﻿using System;
using UnityEngine;

namespace NotAlone
{
    public class ClickableManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] clickablesInScene;

        private void Start()
        {
            clickablesInScene = GameObject.FindGameObjectsWithTag(GameTags.CLICKABLEPOINT);
            ShowClickables();
        }

        public void ShowClickables(bool isShowing = true)
        {
            foreach (GameObject go in clickablesInScene)
            {
                go.SetActive(isShowing);
            }
        }
    }
}